<?php

namespace BetaMFD\VehicleBundle\Model;


interface UserInterface
{

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId();

}
