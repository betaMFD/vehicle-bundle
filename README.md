

This bundle is primarily for my own purposes and as of this writing, is VERY WIP. It's not really usable yet.

I used to use a small vehicle fuel tracking app ("Gas Cubby") that did all the things I needed it to and it did it simply with little fuss. It was missing a few minor "nice-to-haves", but all in all I was really happy with it.  

Then it was acquired by another company and it turned into Fuelly which is more awkward to use but has fancier graphics. It had an optional subscription service to add receipts and stuff but I didn't see the point.

However, the day it started pushing for me to pay for a subscription instead of it being a background thing was the day I decided I was DONE with Fuelly. It was time to make my own fuel-tracking thing. I'm cool with it being in a browser and not a stand-alone app, I just need something that works, and this way I control the data.

This is meant to be add-and-go since that's what I'm doing on my personal Symfony app, so the entities are actually entities and not models. It's using Doctrine ORM annotations so it only works with Doctrine ORM. I have no intentions on making it work with other things but if someone else stumbles across this and wants to improve it, that's cool.


Composer install directions deliberately left out. Will add in later when this could be useful to someone else. In the mean time the following instructions are mostly so I don't forget steps when hooking it up.



### Enable the Bundle (Symfony 2.8)

Enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new BetaMFD\VehicleBundle\BetaMFDVehicleBundle(),
        );

        // ...
    }

    // ...
}
```

### Enable the Bundle In Symfony 3.4+

Enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:
```php
    BetaMFD\VehicleBundle\BetaMFDVehicleBundle::class => ['all' => true],
```



### Connect UserInterface
Replace 'App\Entity\User' with whatever the path is to your User entity.

```
    orm:
        resolve_target_entities:
            BetaMFD\VehicleBundle\Model\UserInterface: App\Entity\User
```

### Update Database
Create migrations or update your database for the new tables that this bundle brings in.
