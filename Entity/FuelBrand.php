<?php

namespace BetaMFD\VehicleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FuelBrand
 *
 * @ORM\Table(name="betamfd_vehicle_fuel_brand")
 * @ORM\Entity
 */
class FuelBrand
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $brand;

    public function __toString()
    {
        return $this->brand;
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Get the value of Brand
     *
     * @return string
     */
    public function getName()
    {
        return $this->brand;
    }

    /**
     * Set the value of Brand
     *
     * @param string brand
     *
     * @return self
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Set the value of Brand
     *
     * @param string brand
     *
     * @return self
     */
    public function setName($name)
    {
        $this->brand = $name;

        return $this;
    }

}
