<?php

namespace BetaMFD\VehicleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehicle
 *
 * @ORM\Table(name="betamfd_vehicle_vehicle")
 * @ORM\Entity
 */
class Vehicle
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $nickname;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\VehicleBundle\Model\UserInterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    protected $owner;

    /**
     * @var User
     *
     * @ORM\ManyToMany(targetEntity="BetaMFD\VehicleBundle\Model\UserInterface")
     * @ORM\JoinTable(name="betamfd_vehicle_vehicle_drivers")
     */
    protected $drivers;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", length=4, nullable=false)
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $make;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $vin;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $engine;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $transmission;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $licensePlate;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $insurancePolicy;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $tireSize;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $tirePressureFront;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $tirePressureBack;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $disanceUnit;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $fuelUnit;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $trackCityHighway = true;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $driveWiperSize;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $passengerWiperSize;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $backWiperSize;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $oil;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $fuel;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    public function __toString()
    {
        return $this->nickname;
    }

    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param integer id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Nickname
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set the value of Nickname
     *
     * @param string nickname
     *
     * @return self
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get the value of Year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of Year
     *
     * @param integer year
     *
     * @return self
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of Make
     *
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set the value of Make
     *
     * @param string make
     *
     * @return self
     */
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get the value of Model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set the value of Model
     *
     * @param string model
     *
     * @return self
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get the value of Vin
     *
     * @return string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * Set the value of Vin
     *
     * @param string vin
     *
     * @return self
     */
    public function setVin($vin)
    {
        $this->vin = $vin;

        return $this;
    }

    /**
     * Get the value of Engine
     *
     * @return string
     */
    public function getEngine()
    {
        return $this->engine;
    }

    /**
     * Set the value of Engine
     *
     * @param string engine
     *
     * @return self
     */
    public function setEngine($engine)
    {
        $this->engine = $engine;

        return $this;
    }

    /**
     * Get the value of Transmission
     *
     * @return string
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * Set the value of Transmission
     *
     * @param string transmission
     *
     * @return self
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;

        return $this;
    }

    /**
     * Get the value of License Plate
     *
     * @return string
     */
    public function getLicensePlate()
    {
        return $this->licensePlate;
    }

    /**
     * Set the value of License Plate
     *
     * @param string licensePlate
     *
     * @return self
     */
    public function setLicensePlate($licensePlate)
    {
        $this->licensePlate = $licensePlate;

        return $this;
    }

    /**
     * Get the value of Insurance Policy
     *
     * @return string
     */
    public function getInsurancePolicy()
    {
        return $this->insurancePolicy;
    }

    /**
     * Set the value of Insurance Policy
     *
     * @param string insurancePolicy
     *
     * @return self
     */
    public function setInsurancePolicy($insurancePolicy)
    {
        $this->insurancePolicy = $insurancePolicy;

        return $this;
    }

    /**
     * Get the value of Tire Size
     *
     * @return string
     */
    public function getTireSize()
    {
        return $this->tireSize;
    }

    /**
     * Set the value of Tire Size
     *
     * @param string tireSize
     *
     * @return self
     */
    public function setTireSize($tireSize)
    {
        $this->tireSize = $tireSize;

        return $this;
    }

    /**
     * Get the value of Tire Pressure Front
     *
     * @return string
     */
    public function getTirePressureFront()
    {
        return $this->tirePressureFront;
    }

    /**
     * Set the value of Tire Pressure Front
     *
     * @param string tirePressureFront
     *
     * @return self
     */
    public function setTirePressureFront($tirePressureFront)
    {
        $this->tirePressureFront = $tirePressureFront;

        return $this;
    }

    /**
     * Get the value of Tire Pressure Back
     *
     * @return string
     */
    public function getTirePressureBack()
    {
        return $this->tirePressureBack;
    }

    /**
     * Set the value of Tire Pressure Back
     *
     * @param string tirePressureBack
     *
     * @return self
     */
    public function setTirePressureBack($tirePressureBack)
    {
        $this->tirePressureBack = $tirePressureBack;

        return $this;
    }

    /**
     * Get the value of Country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of Country
     *
     * @param string country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the value of Disance Unit
     *
     * @return string
     */
    public function getDisanceUnit()
    {
        return $this->disanceUnit;
    }

    /**
     * Set the value of Disance Unit
     *
     * @param string disanceUnit
     *
     * @return self
     */
    public function setDisanceUnit($disanceUnit)
    {
        $this->disanceUnit = $disanceUnit;

        return $this;
    }

    /**
     * Get the value of Fuel Unit
     *
     * @return string
     */
    public function getFuelUnit()
    {
        return $this->fuelUnit;
    }

    /**
     * Set the value of Fuel Unit
     *
     * @param string fuelUnit
     *
     * @return self
     */
    public function setFuelUnit($fuelUnit)
    {
        $this->fuelUnit = $fuelUnit;

        return $this;
    }

    /**
     * Get the value of Track City Highway
     *
     * @return boolean
     */
    public function getTrackCityHighway()
    {
        return $this->trackCityHighway;
    }

    /**
     * Set the value of Track City Highway
     *
     * @param boolean trackCityHighway
     *
     * @return self
     */
    public function setTrackCityHighway($trackCityHighway)
    {
        $this->trackCityHighway = $trackCityHighway;

        return $this;
    }

    /**
     * Get the value of Drive Wiper Size
     *
     * @return string
     */
    public function getDriveWiperSize()
    {
        return $this->driveWiperSize;
    }

    /**
     * Set the value of Drive Wiper Size
     *
     * @param string driveWiperSize
     *
     * @return self
     */
    public function setDriveWiperSize($driveWiperSize)
    {
        $this->driveWiperSize = $driveWiperSize;

        return $this;
    }

    /**
     * Get the value of Passenger Wiper Size
     *
     * @return string
     */
    public function getPassengerWiperSize()
    {
        return $this->passengerWiperSize;
    }

    /**
     * Set the value of Passenger Wiper Size
     *
     * @param string passengerWiperSize
     *
     * @return self
     */
    public function setPassengerWiperSize($passengerWiperSize)
    {
        $this->passengerWiperSize = $passengerWiperSize;

        return $this;
    }

    /**
     * Get the value of Back Wiper Size
     *
     * @return string
     */
    public function getBackWiperSize()
    {
        return $this->backWiperSize;
    }

    /**
     * Set the value of Back Wiper Size
     *
     * @param string backWiperSize
     *
     * @return self
     */
    public function setBackWiperSize($backWiperSize)
    {
        $this->backWiperSize = $backWiperSize;

        return $this;
    }

    /**
     * Get the value of Oil
     *
     * @return string
     */
    public function getOil()
    {
        return $this->oil;
    }

    /**
     * Set the value of Oil
     *
     * @param string oil
     *
     * @return self
     */
    public function setOil($oil)
    {
        $this->oil = $oil;

        return $this;
    }

    /**
     * Get the value of Fuel
     *
     * @return string
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * Set the value of Fuel
     *
     * @param string fuel
     *
     * @return self
     */
    public function setFuel($fuel)
    {
        $this->fuel = $fuel;

        return $this;
    }

    /**
     * Get the value of Notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set the value of Notes
     *
     * @param string notes
     *
     * @return self
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

}
