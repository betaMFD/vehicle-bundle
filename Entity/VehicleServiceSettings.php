<?php

namespace BetaMFD\VehicleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VehicleServiceSettings
 *
 * @ORM\Table(name="betamfd_vehicle_service_settings",
 *      uniqueConstraints={ @ORM\UniqueConstraint(
 *        columns={"vehicle_id", "service_id"})
 *    })
 * @ORM\Entity
 */
class VehicleServiceSettings
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\VehicleBundle\Entity\Vehicle")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $vehicle;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\VehicleBundle\Entity\Service")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $service;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $miles;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $months;


    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Vehicle
     *
     * @return integer
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set the value of Vehicle
     *
     * @param integer vehicle
     *
     * @return self
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get the value of Service
     *
     * @return integer
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set the value of Service
     *
     * @param integer service
     *
     * @return self
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get the value of Miles
     *
     * @return integer
     */
    public function getMiles()
    {
        return $this->miles;
    }

    /**
     * Set the value of Miles
     *
     * @param integer miles
     *
     * @return self
     */
    public function setMiles($miles)
    {
        $this->miles = $miles;

        return $this;
    }

    /**
     * Get the value of Months
     *
     * @return integer
     */
    public function getMonths()
    {
        return $this->months;
    }

    /**
     * Set the value of Months
     *
     * @param integer months
     *
     * @return self
     */
    public function setMonths($months)
    {
        $this->months = $months;

        return $this;
    }

}
