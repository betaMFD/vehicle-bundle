<?php

namespace BetaMFD\VehicleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="betamfd_vehicle_service")
 * @ORM\Entity
 */
class Service
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $service;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $defaultMiles;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $defaultMonths;


    public function __toString()
    {
        return $this->service;
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Service
     *
     * @return string
     */
    public function getName()
    {
        return $this->service;
    }

    /**
     * Set the value of Service
     *
     * @param string service
     *
     * @return self
     */
    public function setName($name)
    {
        $this->service = $name;

        return $this;
    }

    /**
     * Get the value of Service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set the value of Service
     *
     * @param string service
     *
     * @return self
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get the value of Default Miles
     *
     * @return integer
     */
    public function getDefaultMiles()
    {
        return $this->defaultMiles;
    }

    /**
     * Set the value of Default Miles
     *
     * @param integer defaultMiles
     *
     * @return self
     */
    public function setDefaultMiles($defaultMiles)
    {
        $this->defaultMiles = $defaultMiles;

        return $this;
    }

    /**
     * Get the value of Default Months
     *
     * @return integer
     */
    public function getDefaultMonths()
    {
        return $this->defaultMonths;
    }

    /**
     * Set the value of Default Months
     *
     * @param integer defaultMonths
     *
     * @return self
     */
    public function setDefaultMonths($defaultMonths)
    {
        $this->defaultMonths = $defaultMonths;

        return $this;
    }

}
