<?php

namespace BetaMFD\VehicleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * VehicleFuelUp
 *
 * @ORM\Table(name="betamfd_vehicle_vehicle_fuel_up")
 * @ORM\Entity
 */
class VehicleFuelUp extends VehicleService
{
    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=8, scale=3, nullable=true)
     */
    private $costPerGallon;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=8, scale=3, nullable=true)
     */
    private $gallons;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $partial = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $reset = false;

    /**
     * @var string
     *
     * @ORM\Column(name="hdc_percentage", type="decimal", precision=3, scale=2, nullable=true)
     * @Assert\Range(
     *      min = 0,
     *      max = 1,
     *      minMessage = "You must have a positive number",
     *      maxMessage = "Please enter a decimal between 0 and 1"
     * )
     */
    private $cityPercentage;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $fuelType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $fuelBrand;


    /**
     * Get the value of Cost Per Gallon
     *
     * @return string
     */
    public function getCostPerGallon()
    {
        return $this->costPerGallon;
    }

    /**
     * Set the value of Cost Per Gallon
     *
     * @param string costPerGallon
     *
     * @return self
     */
    public function setCostPerGallon($costPerGallon)
    {
        $this->costPerGallon = $costPerGallon;

        return $this;
    }

    /**
     * Get the value of Gallons
     *
     * @return string
     */
    public function getGallons()
    {
        return $this->gallons;
    }

    /**
     * Set the value of Gallons
     *
     * @param string gallons
     *
     * @return self
     */
    public function setGallons($gallons)
    {
        $this->gallons = $gallons;

        return $this;
    }

    /**
     * Get the value of Partial
     *
     * @return boolean
     */
    public function getPartial()
    {
        return $this->partial;
    }

    /**
     * Set the value of Partial
     *
     * @param boolean partial
     *
     * @return self
     */
    public function setPartial($partial)
    {
        $this->partial = $partial;

        return $this;
    }

    /**
     * Get the value of Reset
     *
     * @return boolean
     */
    public function getReset()
    {
        return $this->reset;
    }

    /**
     * Set the value of Reset
     *
     * @param boolean reset
     *
     * @return self
     */
    public function setReset($reset)
    {
        $this->reset = $reset;

        return $this;
    }

    /**
     * Get the value of City Percentage
     *
     * @return string
     */
    public function getCityPercentage()
    {
        return $this->cityPercentage;
    }

    /**
     * Set the value of City Percentage
     *
     * @param string cityPercentage
     *
     * @return self
     */
    public function setCityPercentage($cityPercentage)
    {
        $this->cityPercentage = $cityPercentage;

        return $this;
    }

    /**
     * Get the value of Fuel Type
     *
     * @return string
     */
    public function getFuelType()
    {
        return $this->fuelType;
    }

    /**
     * Set the value of Fuel Type
     *
     * @param string fuelType
     *
     * @return self
     */
    public function setFuelType($fuelType)
    {
        $this->fuelType = $fuelType;

        return $this;
    }

    /**
     * Get the value of Fuel Brand
     *
     * @return string
     */
    public function getFuelBrand()
    {
        return $this->fuelBrand;
    }

    /**
     * Set the value of Fuel Brand
     *
     * @param string fuelBrand
     *
     * @return self
     */
    public function setFuelBrand($fuelBrand)
    {
        $this->fuelBrand = $fuelBrand;

        return $this;
    }

}
