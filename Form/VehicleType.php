<?php

namespace BetaMFD\VehicleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VehicleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nickname');
        $builder->add('year');
        $builder->add('make');
        $builder->add('model');
        $builder->add('vin');
        $builder->add('engine');
        $builder->add('transmission');
        $builder->add('licensePlate');
        $builder->add('insurancePolicy');
        $builder->add('tireSize');
        $builder->add('tirePressureFront');
        $builder->add('tirePressureBack');
        $builder->add('country');
        $builder->add('disanceUnit');
        $builder->add('fuelUnit');
        $builder->add('trackCityHighway');
        $builder->add('driveWiperSize');
        $builder->add('passengerWiperSize');
        $builder->add('backWiperSize');
        $builder->add('oil');
        $builder->add('fuel');
        $builder->add('notes');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'BetaMFD\VehicleBundle\Vehicle', ));
    }
}
